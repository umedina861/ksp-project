import json
from utils.pg8000_connect import pg8000Connect

def main(event, context):
    return {
        "statusCode": 200,
        "headers": {
            'Access-Control-Allow-Origin': '*'
        },
        "body": json.dumps({
            "result": pg8000Connect.get_employees()
        }),
    }