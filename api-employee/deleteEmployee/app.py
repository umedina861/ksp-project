import json
from utils.pg8000_connect import pg8000Connect

def main(event, context):
    employee_id = event['pathParameters']['employee_id']
    employee_id = pg8000Connect.delete_employee(employee_id)
    
    response = {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps({'employee_id': employee_id})
    }
    
    return response
    