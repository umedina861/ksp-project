import pg8000
import os
import pandas as pd
import json
import base64
import random
from datetime import datetime,timedelta
import pytz

class pg8000Connect:
    def connect():
        try:
            connect = pg8000.connect(
                                host=os.environ['PSQL_HOST'],
                                port=os.environ['PSQL_PORT'],
                                user=os.environ['PSQL_USER'],
                                password=os.environ['PSQL_PASS'],
                                database=os.environ['PSQL_DB']
                              )
            return connect
        except:
            print("Error de base de datos, favor de revisar")
                
    def get_employees():
        try:
            connect = pg8000Connect.connect()
            c = connect.cursor()
            sql = """SELECT e.employee_id , e.name, e.last_name, e.photo ,e.salary ,
                e.position_id, p.position ,e.status, contract_date,
                b.name AS name_b, b.last_name AS last_name_b,
                b.gender, b.relationship, b.birthday 
                FROM employees e 
                INNER JOIN positions p USING(position_id)
                INNER JOIN beneficiaries b USING(employee_id)
                ORDER BY e.employee_id """
            c.execute(sql)
            result = c.fetchall()
            df_res = pd.DataFrame(result, columns=['employee_id', 'name', 'last_name', 'photo', 'salary', 'position_id', 'position', 'status','contract_date','name_b','last_name_b','gender','relationship','birthday'])
            df_res = df_res.fillna(0) 
            df_res['contract_date'] = df_res['contract_date'].astype(str)
            df_res['birthday'] = df_res['birthday'].astype(str)
            json_res = json.loads(df_res.to_json(orient='records'))
            str1 = str(json_res)
            byte_string = bytes(str1, 'utf8')
            b64str = base64.b64encode(byte_string)
            return json_res
        except Exception as e: 
            print ( e )
        finally:
            c.close()
            connect.close()
            
    def delete_employee(employee_id):
        try:
            connect = pg8000Connect.connect()
            c = connect.cursor()
            sql = """DELETE FROM beneficiaries WHERE employee_id = '%s'""" % (employee_id)
            c.execute(sql)
            print(sql)
            connect.commit()
            
            sql = """DELETE FROM employees WHERE employee_id = '%s'""" % (employee_id)
            c.execute(sql)
            print(sql)
            connect.commit()
            
            return employee_id
        except Exception as e: 
            print ( e )
        finally:
            c.close()
            connect.close()
    
    def update_employee(data):
        try:
            connect = pg8000Connect.connect()
            c = connect.cursor()
            sql ="""UPDATE employees SET name='%s', last_name='%s', position_id='%s', salary='%s', status='%s' WHERE employee_id = '%s'""" % (data['name'],data['last_name'],data['position'],data['salary'],data['position'],data['employee_id'])
            c.execute(sql)
            print(sql)
            connect.commit()
            sql = """UPDATE beneficiaries SET name='%s', last_name='%s', relationship='%s', gender='%s', birthday='%s' WHERE employee_id = '%s'""" % (data['name_b'],data['last_name_b'],data['relationship'],data['gender'],data['birthday'],data['employee_id'])
            c.execute(sql)
            print(sql)
            connect.commit()
            return data['employee_id']
        except Exception as e: 
            print ( e )
        finally:
            c.close()
            connect.close()
            
    def save_employee(data):
        try:
            connect = pg8000Connect.connect()
            photo = str("https://robohash.org/" + str(random.randint(0,100)))
            time_zone_mx = pytz.timezone('America/Mexico_City')
            now = datetime.now(time_zone_mx)
            date = now.strftime('%Y-%m-%d')
            c = connect.cursor()
            print(data)
            sql ="""INSERT INTO employees (name, last_name, photo, position_id, salary, status, contract_date) 
                    VALUES('%s','%s','%s','%s','%s','%s','%s')""" % (data['name'],data['last_name'],photo,data['position'],data['salary'],'ACTIVO',date)
            c.execute(sql)
            print(sql)
            connect.commit()
            
            sql = """SELECT employee_id 
                FROM employees
                ORDER BY employee_id DESC 
                LIMIT 1"""
            c.execute(sql)
            result = c.fetchone()
            employee_id = result[0]
            
            sql = """INSERT INTO beneficiaries (employee_id, name, last_name, relationship, gender, birthday) VALUES('%s', '%s', '%s', '%s', '%s', '%s')""" % (employee_id,data['name_b'],data['last_name_b'],data['relationship'],data['gender'],data['birthday'])
            c.execute(sql)
            connect.commit()
            return employee_id
        except Exception as e: 
            print ( e )
        finally:
            c.close()
            connect.close()