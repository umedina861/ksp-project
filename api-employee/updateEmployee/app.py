import json
from utils.pg8000_connect import pg8000Connect

def main(event, context):
    data = json.loads(event['body'])
    employee_id = pg8000Connect.update_employee(data)
    
    response = {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps({'employee_id': employee_id})
    }
    
    return response
    