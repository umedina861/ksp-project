import React, { useState }from 'react'
import {Table, Button, ListGroup, Modal, Form, Alert} from 'react-bootstrap';
import {getEmployeesData} from './getEmployees'
import axios from 'axios';

const TableEmployees = () => {
    const [show, setShow] = useState(false);
    const [update, setUpdate] = useState(false);
  const [form, setValue] = useState({
    employee_id: "",
    name: "",
    last_name: "",
    position: 1,
    salary: "",
    status:"",
    name_b:"",
    last_name_b:"",
    relationship:"",
    gender:"H",
    birthday:""
  });
  
  
  const handleCloseUpdate = () => setUpdate(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleShowUpdate = (data) => {
    setUpdate(true);
    setValue(data);
}

const handleDelete = async (employee_id) => {
    await axios.delete('https://29svbhl88d.execute-api.us-east-1.amazonaws.com/delete-employee/' + employee_id,)
        .then(res => {
          window.location.reload(false);
        })
}

const handleUpdate = async () => {
    console.log(form);
    await axios.put('https://29svbhl88d.execute-api.us-east-1.amazonaws.com/update-employee', { 
        "employee_id":form.employee_id,
        "name":form.name,
        "last_name":form.last_name,
        "position":form.position,
        "salary":form.salary,
        "status":form.status,
        "name_b":form.name_b,
        "last_name_b":form.last_name_b,
        "relationship":form.relationship,
        "gender":form.gender,
        "birthday":form.birthday
       },)
        .then(res => {
           window.location.reload(false);
        })
}

  const handleChange = e => {
    console.log(e.target.name);
    setValue({ ...form, [e.target.name]: e.target.value });
    console.log(form)
  };

  const handleInsert = async () => {
    console.log(form)
    await axios.post('https://29svbhl88d.execute-api.us-east-1.amazonaws.com/save-employee', { 
      "name":form.name,
      "last_name":form.last_name,
      "position":form.position,
      "salary":form.salary,
      "status":form.status,
      "name_b":form.name_b,
      "last_name_b":form.last_name_b,
      "relationship":form.relationship,
      "gender":form.gender,
      "birthday":form.birthday
     },)
      .then(res => {
        window.location.reload(false);
      })
  };
    const [employees, setEmployees] = useState({});
    const getEmployees = async(e) => {
        if (Object.keys(employees).length === 0) {
            const data =await getEmployeesData();
            setEmployees(data);
            console.log(data);
        }
    }
    getEmployees();


    return (
        <>
        <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Button variant="primary" onClick={handleShow}>Nuevo Empleado</Button>
              </li>
              <li className="nav-item active">
              </li>
            </ul>
          </div>
        </nav>
      </div>
            <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Nuevo Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
        <Form.Group>
        <Form.Group>
          <Alert key="success" variant="success">Datos Empleado</Alert>
            </Form.Group>
          <Form.Label htmlFor="name">Nombre</Form.Label>
          <Form.Control
            type="input"
            name="name"
            onChange={handleChange}
            value={form.name}
            />
          </Form.Group>
            <Form.Group>
          <Form.Label htmlFor="name">Apellido</Form.Label>
          <Form.Control
            type="input"
            name="last_name"
            onChange={handleChange}
            value={form.last_name}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor="name">Posición</Form.Label>
            <Form.Select aria-label="Posición" onChange={handleChange}
            value={form.position} name="position">
            <option value="1">Desarrollador</option>
            <option value="2">Administrativo</option>
            <option value="3">Recursos Humanos</option>
          </Form.Select>
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Salario</Form.Label>
          <Form.Control
            type="input"
            name="salary"
            onChange={handleChange}
            value={form.salary}
            />
          </Form.Group>
          <Form.Label htmlFor="salary"></Form.Label>
          <Form.Group>
          <Alert key="primary" variant="primary">Beneficiarios</Alert>
            </Form.Group>
            <Form.Group>
          <Form.Label htmlFor="salary">Nombre</Form.Label>
            <Form.Control
            type="input"
            name="name_b"
            onChange={handleChange}
            value={form.name_b}
            />
          </Form.Group>
            <Form.Group>
          <Form.Label htmlFor="name">Apellido</Form.Label>
          <Form.Control
            type="input"
            name="last_name_b"
            onChange={handleChange}
            value={form.last_name_b}
            />
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Parentesco</Form.Label>
            <Form.Control
            type="input"
            name="relationship"
            onChange={handleChange}
            value={form.relationship}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor="gender">Genero</Form.Label>
            <Form.Select aria-label="Posición" onChange={handleChange}
            value={form.gender} name="gender">
            <option value="H">Hombre</option>
            <option value="M">Mujer</option>
          </Form.Select>
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Fecha de Nacimiento</Form.Label>
            <Form.Control
            type="input"
            name="birthday"
            onChange={handleChange}
            value={form.birthday}
            />
          </Form.Group>
        </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={handleInsert}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={update} onHide={handleCloseUpdate}>
        <Modal.Header closeButton>
          <Modal.Title>Actualizar Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group>
                <Alert key="success" variant="success">Datos Empleado</Alert>
            </Form.Group>
            <Form.Group>
                <img src={form.photo} alt="" width="50" height="50"></img>
            </Form.Group>
            <Form.Group>
            <Form.Label htmlFor="salary">Nombre</Form.Label>
                <Form.Control
                type="input"
                name="name"
                onChange={handleChange}
                value={form.name}
                />
            </Form.Group>
                <Form.Group>
            <Form.Label htmlFor="name">Apellido</Form.Label>
            <Form.Control
                type="input"
                name="last_name"
                onChange={handleChange}
                value={form.last_name}
                />
            </Form.Group>
            <Form.Group>
            <Form.Label htmlFor="name">Posición</Form.Label>
            <Form.Select aria-label="Posición" onChange={handleChange}
            value={form.position} name="position">
            <option value="1">Desarrollador</option>
            <option value="2">Administrativo</option>
            <option value="3">Recursos Humanos</option>
          </Form.Select>
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Salario</Form.Label>
          <Form.Control
            type="input"
            name="salary"
            onChange={handleChange}
            value={form.salary}
            />
          </Form.Group>
          <Form.Label htmlFor="salary"></Form.Label>
          <Form.Group>
          <Alert key="primary" variant="primary">Beneficiarios</Alert>
            </Form.Group>
            <Form.Group>
          <Form.Label htmlFor="salary">Nombre</Form.Label>
            <Form.Control
            type="input"
            name="name_b"
            onChange={handleChange}
            value={form.name_b}
            />
          </Form.Group>
            <Form.Group>
          <Form.Label htmlFor="name">Apellido</Form.Label>
          <Form.Control
            type="input"
            name="last_name_b"
            onChange={handleChange}
            value={form.last_name_b}
            />
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Parentesco</Form.Label>
            <Form.Control
            type="input"
            name="relationship"
            onChange={handleChange}
            value={form.relationship}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor="gender">Genero</Form.Label>
            <Form.Select aria-label="Posición" onChange={handleChange}
            value={form.gender} name="gender">
            <option value="H">Hombre</option>
            <option value="M">Mujer</option>
          </Form.Select>
          </Form.Group>
          <Form.Group>
          <Form.Label htmlFor="salary">Fecha de Nacimiento</Form.Label>
            <Form.Control
            type="input"
            name="birthday"
            onChange={handleChange}
            value={form.birthday}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor="gender">Estatus</Form.Label>
            <Form.Select aria-label="Posición" onChange={handleChange}
            value={form.status} name="status">
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </Form.Select>
          </Form.Group>
            
        </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseUpdate}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={handleUpdate}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>

            <Table>
            <thead>
                <tr>
                <th>#</th>
                <th></th>
                <th>Nombre</th>
                <th>Puesto</th>
                <th>Salario</th>
                <th>Estatus</th>
                <th>Fecha de Contratación</th>
                <th>Beneficiario</th>
                <th></th>
                </tr>
            </thead>
          <tbody>
          {
                Object.keys(employees).map((item, index) => {
                    return (
                        <tr key={index}>
                            <td>
                                {employees[index].employee_id}
                            </td>
                            <td>
                            <img src={employees[index].photo} alt="" width="50" height="50"></img>
                            </td>
                            <th scope="row">
                                {employees[index].name} {employees[index].last_name}
                            </th>    
                            <td>
                                {employees[index].position}
                            </td>
                            <td>
                                ${employees[index].salary}
                            </td>
                            <td>
                                {employees[index].status}
                            </td>
                            <td>
                                {employees[index].contract_date}
                            </td>
                            <td>
                            <ListGroup as="ul">
                                <ListGroup.Item as="li" active>
                                    {employees[index].name_b} {employees[index].last_name_b}
                                </ListGroup.Item>
                                <ListGroup.Item as="li">{employees[index].gender}</ListGroup.Item>
                                <ListGroup.Item as="li">{employees[index].relationship}</ListGroup.Item>
                                <ListGroup.Item as="li">{employees[index].birthday}</ListGroup.Item>
                            </ListGroup>
                            </td>
                            <td>
                            <Button variant="info" onClick={() => handleShowUpdate(employees[index])}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                            </Button>
                            <Button variant="danger" onClick={() => handleDelete(employees[index].employee_id)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                            </svg>
                            </Button>
                            </td>
                        </tr>
                    )
                    })
            }
          </tbody>
        </Table>


        </>
    )
}

export default TableEmployees