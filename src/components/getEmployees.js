import axios from 'axios';

export const getEmployeesData = async () => {
    const { data } = await axios.get('https://29svbhl88d.execute-api.us-east-1.amazonaws.com/get-employees')
    .then(res => {
     return res;
    });
    
    return data.result;
}