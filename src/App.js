import './App.css';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import Header from './components/Header';
import Footer from './components/Footer';
import TableEmployees from './components/TableEmployees';
import Container from 'react-bootstrap/Container';

const App = () => {
  return (
    <Container>
      <Header/>
      <TableEmployees/>
      {/* <Footer/> */}
    </Container>
  );
}

export default App;
